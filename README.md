# Desafio Técnico Let's Code

Neste desafio, eu implementei o back end da aplicação Kanban. A api foi desenvolvida em node js e com a biblioteca express js. O banco de dados utilizado é o postgres. Utilizei docker para rodar as duas aplicações, e há um arquivo docker-compose que automatiza a execução do banco de dados e da API. Como ferramenta ORM, utilizei o Sequelize. Há um script de migração que é executado quando o projeto é iniciado e que garante que as tabelas necessárias serão devidamente criadas.

A api é exposta na porta 5000, e o banco de dados na porta 54322.

## Rodando o projeto
Na raiz do projeto, basta executar o seguinte comando:
```shell
> docker-compose up --build 
```

## Linter

Nesse projeto, configurei o ESLINT como ferramenta de lint. Configurei um script no package.json para executá-lo. Uma vez o projeto rodando, o eslint pode ser executado da seguinte maneira:
```shell
> docker-compose exec api npm run lint
```

## Testes
Para os testes, eu utilizei a biblioteca mocha. Os testes podem ser executado assim:
```shell
> docker-compose exec api npm run test
```

## Observações

Em relação aos testes, não tive muito tempo para trabalhar nessa parte, então não consegui cobertura de todos os cenários, e talvez essa parte poderia ficar um pouco mais organizada. 

Também gostaria de ter conseguido incorporar o front-end no docker-compose, mas não tive tempo suficiente para isso. 

Espero que gostem do projeto!
