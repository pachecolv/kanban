const supertest = require('supertest');
const { expect } = require('chai');
const dotenv = require('dotenv');
// const proxyquire =  require('proxyquire')
const mockery = require('mockery');
const SequelizeMock = require('sequelize-mock-v5');

HTTP_OK = 200
HTTP_CREATED = 201
HTTP_BAD_REQUEST = 400
HTTP_UNAUTHORIZED = 401


// Setup db connection
let dbMock = new SequelizeMock();

// Cards model
let cardMock = dbMock.define('cards', {
        id: '4d1bc104-86d1-4e8e-81ba-84145ec470df',
        titulo: 'b',
        conteudo: 'c',
        lista: 'd'
    },
    {
        instanceMethods: {
            responseFields: function () {
                return {
                    id: this.id,
                    titulo: this.titulo,
                    conteudo: this.conteudo,
                    lista: this.lista
                };
            }
        },
    }
);


cardMock.$queryInterface.$useHandler(function(query, queryOptions, done) {
    if (query === 'findAll') {
        return [
            cardMock.build({
                id: 'bf679813-f107-41af-b66d-e99a8850df31',
                titulo: 'Titulo 1',
                conteudo: 'Conteudo 1',
                lista: 'Lista 1'}),
            cardMock.build({
                id: '74ae9ac2-61ed-4571-a2bb-a4b1d808337d',
                titulo: 'Titulo 2',
                conteudo: 'Conteudo 2',
                lista: 'Lista 2'}),
        ];
    } else if (query === 'findByPk') {
        return  cardMock.build({
            id: '43240314-a421-415f-b4c2-cda0e0fee93d',
            titulo: 'meu titulo',
            conteudo: 'meu conteudo',
            lista: 'minha lista'
        });
    } else {
        return;
    }
});


const modelsMock = {
    Card: cardMock
}

mockery.enable({
    // useCleanCache: true
})  

mockery.registerMock('../models', modelsMock);
mockery.registerAllowable('../src/routes');
mockery.warnOnUnregistered(false);

const app = require('../src/app.js')


describe('CARDS', function () {
    const request = supertest(app);
    let token;

    beforeEach(async function () {
        const response = await request
                            .post('/login')
                            .send({
                                login: process.env.API_LOGIN,
                                senha: process.env.API_PASSWORD
                            });
        token = response.body;
    });

    describe('GET /cards', async function () {

        it('No token', async function () {
            const response = await request.get('/cards');
            expect(response.status).to.be.eql(HTTP_UNAUTHORIZED);

        });

        it('Get cards', async function () { 
            const response = await request.get('/cards')
                                .set('Authorization', `Bearer ${token}`)

            expect(response.status).to.be.eql(200);
            expect(response.body.length).to.be.eql(2);
        });

    });

    describe('POST /cards', async function () {

        it('No token', async function () {
            const response = await request.post('/cards');
            expect(response.status).to.be.eql(HTTP_UNAUTHORIZED);

        });

        it('No card passed', async function () { 
            const response = await request.post('/cards')
                                .set('Authorization', `Bearer ${token}`)
                                .send({});

            expect(response.status).to.be.eql(HTTP_BAD_REQUEST);

        });

        it('Missing LISTA', async function () {
            const payload = {
                titulo: 'meu titulo',
                conteudo: 'meu conteudo'
            };
            const response = await request.post('/cards')
                                .set('Authorization', `Bearer ${token}`)
                                .send(payload);

            expect(response.status).be.be.eql(HTTP_BAD_REQUEST);
        });

        it('Create card', async function () {
            const payload = {
                titulo: 'meu titulo',
                conteudo: 'meu conteudo',
                lista: 'minha lista'
            };
            const response = await request.post('/cards')
                                .set('Authorization', `Bearer ${token}`)
                                .send(payload);

            expect(response.status).to.be.eql(HTTP_CREATED);

        });

    });

    describe('PUT /cards', async function (){

        it('No token', async function () {
            const uuid = '43240314-a421-415f-b4c2-cda0e0fee93d'
            const response = await request.put('/cards/');
            expect(response.status).to.be.eql(HTTP_UNAUTHORIZED);
        });

        it('Different ids', async function () {
            const uuid = '43240314-a421-415f-b4c2-cda0e0fee93d'
            const payload = {
                id: 123,
                titulo: 'meu titulo',
                conteudo: 'meu conteudo',
                lista: 'minha lista'
            };
        
            const response = await request.put(`/cards/${uuid}`)
                                .set('Authorization', `Bearer ${token}`)
                                .send(payload);

            expect(response.status).to.be.eql(HTTP_BAD_REQUEST);
        })

        it('Valid put request', async function () {
            const uuid = '43240314-a421-415f-b4c2-cda0e0fee93d';
            const payload = {
                id: uuid,
                titulo: 'meu titulo',
                conteudo: 'meu conteudo',
                lista: 'minha lista'
            };

            const response = await request.put(`/cards/${uuid}`)
                                .set('Authorization', `Bearer ${token}`)
                                .send(payload);

            expect(response.status).to.be.eql(HTTP_OK);

        });

    });

});