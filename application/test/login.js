const supertest = require('supertest');
const { expect } = require('chai');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

const app = require('../src/app.js')


HTTP_BAD_REQUEST = 400
HTTP_UNAUTHORIZED = 401
HTTP_OK = 200

describe('POST /login', function () {

    const request = supertest(app);

    it('No credentials', async function () {
        const response = await request.post('/login');
        expect(response.status).to.eql(HTTP_BAD_REQUEST);
    });

    it('Pass only login and omit password', async function () {
        const response = await request
                            .post('/login')
                            .send({login: 'some_login'});
        expect(response.status).to.eql(HTTP_BAD_REQUEST);
    });

    it('Empty credentials', async function () {
        const response = await request
                            .post('/login')
                            .send({login: null, senha: null});
        expect(response.status).to.eql(HTTP_UNAUTHORIZED);
    });

    it('Wrong credentials', async function () {
        const response = await request
                            .post('/login')
                            .send({login: 'algum_login', senha: 'alguma_senha'});
        expect(response.status).to.eql(HTTP_UNAUTHORIZED);
    });

    it('Valid credentials', async function () {
        const response = await request
                            .post('/login')
                            .send({
                                login: process.env.API_LOGIN,
                                senha: process.env.API_PASSWORD
                            });
        expect(response.status).to.eql(HTTP_OK);
        expect(response.body).to.be.a('string');
        expect(jwt.verify(response.body, process.env.JWT_SECRET)).not.to.throw;
    });

});