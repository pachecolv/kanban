const express = require('express');
const uuid = require('uuid');

const { authToken } = require('../middlewares');
const { Card } = require('../models');
const { cardLogger } = require('../modules/logger');

const router = express.Router();
router.use(authToken);

// GET cards
router.get('/', async (req, res, next) => {
    try {
        const cards = await Card.findAll({ attributes: ['id', 'titulo', 'conteudo', 'lista'] });

        res.json(cards);
    } catch (e) {
        res.sendStatus(500);
    }
});

// POST cards
router.post('/', async (req, res, next) => {
    try {
    // Check request body
        const { titulo, conteudo, lista } = req.body;
        if (!titulo || !conteudo || !lista) {
            res.status(400).send('Fields titulo, conteudo and lista are mandatory');
            return;
        }

        // Create card in database
        const newCard = await Card.create({
            titulo: titulo,
            conteudo: conteudo,
            lista: lista
        });

        res.status(201).json(newCard.responseFields);
    } catch (e) {
        res.sendStatus(500);
    }
});

// PUT cards
router.put('/:id', async (req, res, next) => {
    try {
    // Check request body and id
        const { id: urlId } = req.params;
        const { id, titulo, conteudo, lista } = req.body;

        if (!id || !titulo || !conteudo || !lista) {
            res.status(400).send('Missing fields in request body.');
            return;
        }

        if (id !== urlId) {
            res.status(400).send('Id in URL is different from id in request body.');
            return;
        }

        if (!uuid.validate(id)) {
            res.sendStatus(404);
            return;
        }

        // Retrieve card
        const card = await Card.findByPk(urlId);
        if (card == null) {
            res.sendStatus(404);
            return;
        }

        // Update card
        Object.assign(card, { titulo, conteudo, lista });
        await card.save();

        cardLogger(req.method, card.id, card.titulo);

        res.json(card.responseFields);
    } catch (e) {
        res.sendStatus(500);
    }
});

// DELETE cards
router.delete('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        if (!uuid.validate(id)) {
            res.sendStatus(404);
            return;
        }

        const card = await Card.findByPk(id);

        const nDeleted = await Card.destroy({ where: { id } });
        if (nDeleted === 0) {
            res.sendStatus(404);
            return;
        }

        let allCards = await Card.findAll();
        allCards = allCards.map((card) => { return card.responseFields; });

        cardLogger(req.method, card.id, card.titulo);

        res.json(allCards);
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;
