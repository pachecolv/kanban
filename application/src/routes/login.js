const express = require('express');
const router = express.Router();

const { validateCredentials, createToken } = require('../modules/auth');

router.post('/', (req, res, next) => {
    // Check body mandatory fields
    if (req.body == null) {
        res.status(400).send('Body must contain login and senha fields');
    }
    if (!Object.prototype.hasOwnProperty.call(req.body, 'login') || !Object.prototype.hasOwnProperty.call(req.body, 'senha')) {
        res.status(400).send('Body must contain login and senha fields');
        return;
    }

    // Check credentials
    const { login, senha } = req.body;
    if (!validateCredentials(login, senha)) {
        res.status(401).send('Invalid credentials');
        return;
    }

    // Create and return token
    const token = createToken(req.body.login);

    res.json(token);
});

module.exports = router;
