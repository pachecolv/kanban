const { authenticateToken } = require('../modules/auth');

module.exports.authToken = (req, res, next) => {
    // Extract token
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return res.sendStatus(401);

    // Validate token
    if (!authenticateToken(token)) {
        res.sendStatus(401);
        return;
    }

    next();
};
