'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('cards', {
            id: {
                allowNull: false,
                primaryKey: true,
                type: Sequelize.UUID
            },
            titulo: {
                type: Sequelize.STRING,
                allowNull: false
            },
            conteudo: {
                type: Sequelize.TEXT,
                defaultValue: '',
                allowNull: false
            },
            lista: {
                type: Sequelize.STRING,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('cards');
    }
};
