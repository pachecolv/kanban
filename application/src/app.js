const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');

// Load variables from .env file
dotenv.config();

const loginRouter = require('./routes/login');
const cardsRouter = require('./routes/cards');

const app = express();

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use('/login', loginRouter);
app.use('/cards', cardsRouter);

module.exports = app;
