const jwt = require('jsonwebtoken');

module.exports.createToken = (username) => {
    return jwt.sign({ username }, process.env.JWT_SECRET, { expiresIn: '1h' });
};

module.exports.authenticateToken = (token) => {
    try {
        jwt.verify(token, process.env.JWT_SECRET);
    } catch (err) {
        return false;
    }

    return true;
};
