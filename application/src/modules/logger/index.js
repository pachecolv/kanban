const winston = require('winston');
const moment = require('moment-timezone');

const logger = winston.createLogger({
    transports: [new winston.transports.Console()],
    format: winston.format.simple()
});

module.exports.cardLogger = (method, id, title) => {
    // Get string for action
    let action;
    if (method === 'PUT') {
        action = 'Alterado';
    } else if (method === 'DELETE') {
        action = 'Removido';
    } else {
        // Nothing to be logged
        return;
    }

    // Timestamp
    const timestamp = moment().tz('America/Sao_Paulo').format('DD/MM/YYYY HH:mm:ss');

    // Log message
    logger.info(`${timestamp} - Card ${id} - ${title} - ${action}`);
};
