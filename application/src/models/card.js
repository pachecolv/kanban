'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Card extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate (models) {
            // define association here
        }
    };
    Card.init({
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        titulo: {
            type: DataTypes.STRING
        },
        conteudo: {
            type: DataTypes.STRING
        },
        lista: {
            type: DataTypes.STRING
        },
        responseFields: {
            type: DataTypes.VIRTUAL,
            get () {
                return {
                    id: this.id,
                    titulo: this.titulo,
                    conteudo: this.conteudo,
                    lista: this.lista
                };
            }
        }

    }, {
        sequelize,
        modelName: 'Card',
        tableName: 'cards'
    });
    return Card;
};
