FROM node:lts-alpine

WORKDIR /application

COPY ./application /application

RUN npm install
# RUN npx sequelize-cli db:migrate

ENTRYPOINT ["sh", "./docker-entrypoint.sh"]